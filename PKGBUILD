# Maintainer: Nate Levesque <public@thenaterhood.com>
# Contributor: Erik Dubois <erik.dubois@gmail.com>
# Contributor: tinywrkb <tinywrkb@gmail.com>
# Contributor: Vlad M. <vlad@archlinux.net>
# Contributor: Zhengyu Xu <xzy3186@gmail.com>

pkgname=insync
pkgver=3.8.1.50459
_dist=buster
pkgrel=1
pkgdesc="An unofficial Google Drive client that runs on Linux, with support for various desktops"
url="https://www.insynchq.com/pricing?fp_ref=manjariando"
license=('custom:insync')
arch=('x86_64')
options=('!strip' '!emptydirs')
install=$pkgname.install
source=("http://cdn.insynchq.com/builds/linux/${pkgname}_${pkgver}-${_dist}_amd64.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.desktop"
        'insync@.service'
        'insync.service')
sha256sums=('163ed46b99561a3faabbae1bbd2714f314f1c2e32cf01e67ed65c70e1acd1431'
            '2d7bf945e6715a03e2f63e346cf6bf7a5dd1654dfe904e116251ce5bef3ae7af'
            'ace4edeb74c909e3a866d81d9b51cb3854fde4c83eacda5e50c4bf32acb291f4'
            'cf276c1dbf1592ea63a21c2d61c75f7ad6ec3b13e87b3aaa331e9c14799f4598'
            '0c3448718a0a95262ca75fbda3e4a4c38f5e68aee77b27b11b22ca0967030b08')

package() {
    depends=('adobe-source-code-pro-fonts' 'alsa-lib' 'fontconfig' 'freetype2' 'glibc'
            'hicolor-icon-theme' 'libxcrypt' 'libglvnd' 'nss' 'xdg-utils')
    optdepends=('lib32-libappindicator-gtk2: Required for tray icon in some configurations')

    tar xf data.tar.gz
    cp -rp usr ${pkgdir}/
    install -Dm644 ${srcdir}/insync@.service ${pkgdir}/usr/lib/systemd/system/insync@.service
    install -Dm644 ${srcdir}/insync.service ${pkgdir}/usr/lib/systemd/user/insync.service

    # fix
    install -dm755  ${pkgdir}/usr/lib/x86_64-linux-gnu/gdk-pixbuf-2.0/2.10.0
    ln -s /usr/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache ${pkgdir}/usr/lib/x86_64-linux-gnu/gdk-pixbuf-2.0/2.10.0/loaders.cache
    echo "-> Patching https://forums.insynchq.com/t/crash-when-changing-sync-folders-linux-with-solution/17254/4"
    mv ${pkgdir}/usr/lib/insync/libgdk_pixbuf-2.0.so.0{,.bak}
    echo "-> Patching https://forums.insynchq.com/t/crash-changing-sync-directory-with-fix/17364/10"
    mv ${pkgdir}/usr/lib/insync/libpangoft2-1.0.so.0{,.bak}
    echo "-> Patching https://forums.insynchq.com/t/gui-is-broken-on-arch-for-3-7-7-with-workaround/18434"
    mv ${pkgdir}/usr/lib/insync/libstdc++.so.6{,.bak}
    echo "-> Patching https://forums.insynchq.com/t/workaround-for-libcrypt-so-1-issue-on-3-7-9-for-arch/18504"
    ln -s /usr/lib/libcrypt.so.2 ${pkgdir}/usr/lib/insync/libcrypt.so.1

    # Appstream
    rm -rf "${pkgdir}/usr/share/applications/${pkgname}.desktop"
    install -Dm64 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm64 "${srcdir}/com.${pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/com.${pkgname}.metainfo.xml"
}
